#!/bin/sh

################################################################################
# INSTALL VIRTUAL MACHINE AGENT
# Do this on Hetzner to install an agent which allows for resetting the root
# password from the Hetzner web panel.
################################################################################

echo ""
echo "========================================================================="
echo "Installing virutal machine agent..."
echo "========================================================================="

DEBIAN_FRONTEND=noninteractive apt-get install -y qemu-guest-agent


################################################################################
# SETUP SWAP
# 
# Notes: 
# * The recommended value is 2G but based on my testing, it's not enough.
# * Make sure swapfile is in /etc/fstab
################################################################################

echo ""
echo "========================================================================="
echo "Setting up swap..."
echo "========================================================================="

swapoff /swapfile
rm /swapfile
fallocate -l 4G /swapfile
chmod 600 /swapfile
mkswap /swapfile
swapon /swapfile

echo "vm.swappiness=10" | tee -a /etc/sysctl.conf
echo "/swapfile none swap sw 0 0" | tee -a /etc/fstab

sysctl -p


################################################################################
# SETUP FIREWALL
################################################################################

echo ""
echo "========================================================================="
echo "Setting up firewal..."
echo "========================================================================="

ufw default allow outgoing \
  && ufw default deny incoming \
  && ufw allow ssh/tcp \
  && ufw limit ssh/tcp \
  && ufw allow http/tcp \
  && ufw allow https/tcp \
  && ufw allow 9033/tcp \
  && ufw logging on \
  && ufw -f enable
systemctl enable ufw


################################################################################
# INSTALL DOCKER
################################################################################

echo ""
echo "========================================================================="
echo "Installing docker..."
echo "========================================================================="

DEBIAN_FRONTEND=noninteractive apt-get install -y apt-transport-https ca-certificates curl software-properties-common \
  && curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add - \
  && add-apt-repository -y "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable" \
  && apt-get -y update \
  && apt-get install docker-ce


################################################################################
# SETUP NON-ROOT USER
################################################################################

export USERNAME=horizen

echo ""
echo "========================================================================="
echo "Setting up non-root user $USERNAME"
echo "========================================================================="
useradd -ms /bin/bash -G adm,systemd-journal,sudo $USERNAME
passwd $USERNAME


################################################################################
# CREATE ZEN CONFIG FILE
################################################################################

echo ""
echo "========================================================================="
echo "Creating ZEN config file"
echo "========================================================================="
su $USERNAME \
  && cd ~ \
  && mkdir .zen



################################################################################
# SECURE SSH
# Disable SSH login as root.
# 
# Optional. More secure, but you might get lock yourself out of the node.
#
# Important: Run this manually after you add your key to the non-root user 
# authorized_keys file and test ssh connection as the non-root user.
################################################################################

#sed -i  "s/.*PermitRootLogin yes/PermitRootLogin no/g" /etc/ssh/sshd_config &&\
#sed -i  "s/.*PasswordAuthentication yes/PasswordAuthentication no/g" /etc/ssh/sshd_config &&\
#sed -i  "s/.*ChallengeResponseAuthentication yes/ChallengeResponseAuthentication no/g" /etc/ssh/sshd_config
#systemctl restart sshd