#!/bin/bash

export DEBIAN_FRONTEND=noninteractive

print_status() {
  echo
  echo "### $1"
  echo
}

if [ $# -lt 4 ]; then
  echo "Execution format ./install.sh stakeaddr email FQDN region nodetype"
  exit
fi

# Installation variables
stakeaddr=${1}
email=${2}
FQDN=${3}
region=${4}

if [ -z "$5" ]; then
  nodetype="secure"
else
  nodetype=${5}
fi

print_status "Installing the Horizen node..."

echo "FQDN: $FQDN"
echo "Email: $email"
echo "Stake Address: $stakeaddr"


################################################################################
### Create swap
### -
### Notes: 
### * The recommended value is 2G but based on my testing, it's not enough.
### * Make sure swapfile is in /etc/fstab

print_status "Creating a swapfile..."

swapoff /swapfile
rm /swapfile
fallocate -l 4G /swapfile
chmod 600 /swapfile
mkswap /swapfile > /dev/null 2>&1
swapon /swapfile > /dev/null 2>&1

echo "vm.swappiness=10" | tee -a /etc/sysctl.conf > /dev/null 2>&1
# May or may not be necessary. Check your /etc/fstab first.
echo "/swapfile none swap sw 0 0" | tee -a /etc/fstab > /dev/null 2>&1

sysctl -p > /dev/null 2>&1
echo "OK"


################################################################################
### Install necessary packages 

print_status "Populating apt-get cache..."
apt-get update > /dev/null 2>&1
echo "OK"

# Do this on Hetzner or other hosting supporting qemu guest agent
print_status "Installing a virtual hosting agent..."
apt-get install -y qemu-guest-agent > /dev/null 2>&1
echo "OK"

print_status "Installing system packages..."
apt-get install -y apt-transport-https ca-certificates curl software-properties-common curl ufw dnsutils pwgen jq python > /dev/null 2>&1
echo "OK"

rpcuser=$(pwgen -s 32 1)
rpcpassword=$(pwgen -s 64 1)

print_status "Installing docker..."
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add - > /dev/null 2>&1 \
  && add-apt-repository -y "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable" > /dev/null 2>&1 \
  && apt-get -y update > /dev/null 2>&1 \
  && apt-get -y install docker-ce > /dev/null 2>&1

systemctl enable docker > /dev/null 2>&1
systemctl start docker > /dev/null 2>&1
echo "OK"

print_status "Creating the node mount directories..."
mkdir -p /mnt/zen/config /mnt/zen/data /mnt/zen/zcash-params /mnt/zen/certs
echo "OK"

print_status "Disabling apache2 if enabled, to free the port 80..."
systemctl disable apache2 > /dev/null 2>&1
systemctl stop apache2 > /dev/null 2>&1
echo "OK"

print_status "Installing certbot..."
add-apt-repository ppa:certbot/certbot -y > /dev/null 2>&1
apt-get update -y > /dev/null 2>&1
apt-get install certbot -y > /dev/null 2>&1
echo "OK"


################################################################################
### Issue a certificate
### -
### Note: Other steps such as copying the certificate to /usr and ca updating
### are being performed in the docker container.

print_status "Issusing and installing certificate for $FQDN..."
certbot certonly -n --agree-tos --register-unsafely-without-email --standalone -d $FQDN

chmod -R 755 /etc/letsencrypt/

echo \
"[Unit]
Description=zenupdate.service

[Service]
Type=oneshot
ExecStart=/usr/bin/certbot -q renew --deploy-hook 'systemctl restart zen-node && systemctl restart zentracker && docker rmi $(docker images --quiet --filter "dangling=true")'
PrivateTmp=true" | tee /lib/systemd/system/zenupdate.service

echo \
"[Unit]
Description=Run zenupdate unit daily @ 06:00:00 (UTC)

[Timer]
OnCalendar=*-*-* 06:00:00
Unit=zenupdate.service
Persistent=true

[Install]
WantedBy=timers.target" | tee /lib/systemd/system/zenupdate.timer

systemctl stop certbot.timer
systemctl disable certbot.timer

systemctl start zenupdate.service

systemctl start zenupdate.timer
systemctl enable zenupdate.timer
echo "OK"


################################################################################
### Create a config file for the zend service

print_status "Creating the zen config..."

cat <<EOF > /mnt/zen/config/zen.conf
rpcport=18231
rpcallowip=127.0.0.1
rpcworkqueue=512
server=1
# Docker doesn't run as daemon
daemon=0
listen=1
txindex=1
logtimestamps=1
rpcuser=$rpcuser
rpcpassword=$rpcpassword
tlscertpath=/etc/letsencrypt/live/$FQDN/cert.pem
tlskeypath=/etc/letsencrypt/live/$FQDN/privkey.pem
#
port=9033
EOF
echo "OK"

print_status "Trying to determine public ip addresses..."

publicips=$(dig $FQDN A $FQDN AAAA +short)
while read -r line; do
    echo "externalip=$line" >> /mnt/zen/config/zen.conf
done <<< "$publicips"
echo "OK"


################################################################################
### Create a config file for the node tracker

print_status "Creating the node tracker config..."

if [ $nodetype = "super" ]; then
  servers=xns
else
  servers=ts
fi

mkdir -p /mnt/zen/tracker/
cat << EOF > /mnt/zen/tracker/config.json
{
  "active": "$nodetype",
  "$nodetype": {
    "nodetype": "$nodetype",
    "nodeid": null,
    "servers": [
      "${servers}2.eu",
      "${servers}1.eu",
      "${servers}3.eu",
      "${servers}4.eu",
      "${servers}4.na",
      "${servers}3.na",
      "${servers}2.na",
      "${servers}1.na"
    ],
    "stakeaddr": "$stakeaddr",
    "email": "$email",
    "fqdn": "$FQDN",
    "ipv": "4",
    "region": "$region",
    "home": "${servers}1.$region",
    "category": "none"
  }
}
EOF
echo "OK"


################################################################################
### Install zend + tracker services

print_status "Installing zen-node (zend) service..."

cat <<EOF > /etc/systemd/system/zen-node.service
[Unit]
Description=Zen Daemon Container
After=docker.service
Requires=docker.service

[Service]
TimeoutStartSec=10m
Restart=always
ExecStartPre=-/usr/bin/docker stop zen-node
ExecStartPre=-/usr/bin/docker rm zen-node
# Always pull the latest docker image
ExecStartPre=/usr/bin/docker pull prototypehq/zen-node:latest
ExecStart=/usr/bin/docker run --rm --net=host -p 9033:9033 -p 18231:18231 -v /mnt/zen:/mnt/zen -v /etc/letsencrypt/:/etc/letsencrypt/ --name zen-node prototypehq/zen-node:latest
[Install]
WantedBy=multi-user.target
EOF

print_status "Installing zentracker service..."
cat <<EOF > /etc/systemd/system/zentracker.service
[Unit]
Description=Zen Node Tracker Container
After=docker.service
Requires=docker.service

[Service]
TimeoutStartSec=10m
Restart=always
ExecStartPre=-/usr/bin/docker stop zentracker
ExecStartPre=-/usr/bin/docker rm zentracker
# Always pull the latest docker image
ExecStartPre=/usr/bin/docker pull prototypehq/zentracker:latest
#ExecStart=/usr/bin/docker run --init --rm --net=host -v /mnt/zen:/mnt/zen --name zentracker prototypehq/zentracker:latest
ExecStart=/usr/bin/docker run --rm --net=host -v /mnt/zen:/mnt/zen --name zentracker prototypehq/zentracker:latest
[Install]
WantedBy=multi-user.target
EOF
echo "OK"

print_status "Enabling and starting container services..."
systemctl daemon-reload
systemctl enable zen-node
systemctl restart zen-node

systemctl enable zentracker
systemctl restart zentracker
echo "OK"


################################################################################
### Firewall settings

print_status "Enabling basic firewall services..."
ufw default allow outgoing > /dev/null 2>&1
ufw default deny incoming > /dev/null 2>&1
ufw allow ssh/tcp > /dev/null 2>&1
ufw limit ssh/tcp > /dev/null 2>&1
ufw allow http/tcp > /dev/null 2>&1
ufw allow https/tcp > /dev/null 2>&1
ufw allow 9033/tcp > /dev/null 2>&1
#testnet:
#ufw allow 19033/tcp
ufw --force enable
echo "OK"

#print_status "Enabling fail2ban services..."
#systemctl enable fail2ban
#systemctl start fail2ban


################################################################################
### Waiting for zen params
### -
### Fetching the zen params and blockchain is already underway since we've
### enabled the zen container (docker) services a few steps earlier.

print_status "Waiting for node to fetch params ..."
until docker exec -it zen-node gosu zenops zen-cli getinfo
do
  echo "..."
  sleep 30
done


################################################################################
### Obtain node addresses

if [[ $(docker exec -it zen-node gosu zenops zen-cli listaddresses | wc -l) -eq 3 ]]; then
  print_status "Generating a staking address... You need to send 0.05 ZEN to the staking address to pay for performing challenges."
  docker exec -it zen-node gosu zenops zen-cli getnewaddress > /dev/null && docker exec -it zen-node gosu zenops zen-cli listaddresses
  echo "(The first address identifies your node, the second address is the staking address.)"

  echo
  echo "Send 0.05 ZEN to this address:"
  docker exec -it zen-node gosu zenops zen-cli listaddresses | jq -r '.[1]'

  print_status "Restarting the node tracker"
  systemctl restart zentracker
else
  print_status "Node already has a staking address. Send 0.05 ZEN to this address:"
  docker exec -it zen-node gosu zenops zen-cli listaddresses | jq -r '.[1]'
fi

print_status "Installation completed."

cat << EOF

================================================================================
POST-INSTALLATION INSTRUCTIONS
================================================================================

(1) Send a challenge deposit of 0.05 ZEN to the t_address above while
    following the staking instructions from the official documentation.

(2) When the node has synchronized, incl. the challenge deposit transaction,
    split the deposit into two z_addresses:

    a. Run this command to check the balance:
    $ docker exec -it zen-node gosu zenops watch -n 30 zen-cli z_gettotalbalance

    b. Run this command to split the deposit:
    $ docker exec -it zen-node gosu zenops zen-cli z_sendmany \$(docker exec -it zen-node gosu zenops zen-cli listaddresses | jq -r '.[1]') '[{"address": "'\$(docker exec -it zen-node gosu zenops zen-cli z_getnewaddress)'", "amount": 0.0249},{"address": "'\$(docker exec -it zen-node gosu zenops zen-cli z_getnewaddress)'", "amount": 0.0249}]'

(3) Wait for the split transaction to get processed:

    $ docker exec -it zen-node gosu zenops watch -n 30 zen-cli z_gettotalbalance

(4) Once the private balance of 0.0498 is displayed, restart the tracker:

    $ systemctl restart zentracker

(5) Confirm the tracker has picked up the balance, returned a node id and 
    registered the node:

    $ docker logs zentracker

EOF