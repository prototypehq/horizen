#!/bin/bash
set -e


################################################################################
### Add local user (zenops)

# either use the LOCAL_USER_ID if passed at runtime or fallback.
USER_ID=${LOCAL_USER_ID:-9001}
GRP_ID=${LOCAL_GRP_ID:-9001}

getent group user > /dev/null 2>&1 || groupadd -g $GRP_ID zenops
id -u zenops > /dev/null 2>&1 || useradd --shell /bin/bash -u $USER_ID -g $GRP_ID -o -c "" -m zenops

LOCAL_UID=$(id -u zenops)
LOCAL_GID=$(getent group zenops | cut -d ":" -f 3)

if [ ! "$USER_ID" == "$LOCAL_UID" ] || [ ! "$GRP_ID" == "$LOCAL_GID" ]; then
    echo "Warning: User with UID "$LOCAL_UID"/GID "$LOCAL_GID" already exists, most likely this container has been started before with a different UID/GID. Re-create it to change UID/GID."
fi

echo "Starting container with UID/GID: "$(id -u zenops)"/"$(getent group zenops | cut -d ":" -f 3)

export HOME=/home/zenops


################################################################################
### Check the exitence of the zen config file

if [ ! -f "/mnt/zen/config/zen.conf" ]; then
  echo "zen.conf not found. Exiting."
  exit 1
else
  if [ ! -L $HOME/.zen ]; then
    ln -s /mnt/zen/config $HOME/.zen > /dev/null 2>&1 || true
  fi
fi


################################################################################
### Check zcash-params

# zcash-params can be symlinked in from an external volume or created locally
if [ -d "/mnt/zen/zcash-params" ]; then
  if [ ! -L $HOME/.zcash-params ]; then
    echo "Symlinking external zcash-params volume..."
    ln -s /mnt/zen/zcash-params $HOME/.zcash-params > /dev/null 2>&1 || true
  fi
else
  echo "Using local zcash-params folder"
  mkdir -p $HOME/.zcash-params > /dev/null 2>&1 || true
fi

# data folder can be an external volume or created locally
if [ ! -d "/mnt/zen/data" ]; then
  echo "Using local data folder"
  mkdir -p /mnt/zen/data > /dev/null 2>&1 || true
else
  echo "Using external data volume"
fi


################################################################################
### Setup the certificate

# Get the fqdn from the node tracker config file
active=$(cat /mnt/zen/tracker/config.json | jq -r '.active')
if [ $active == "secure" ]; then
  domain=$(cat /mnt/zen/tracker/config.json | jq -r '.secure.fqdn')
else
  domain=$(cat /mnt/zen/tracker/config.json | jq -r '.super.fqdn')
fi

# Perform necessary updates
if [ -f /etc/letsencrypt/live/$domain/chain.pem ]; then
  echo "Copying SSL certificates"
  cp /etc/letsencrypt/live/$domain/chain.pem /usr/local/share/ca-certificates/ca.crt > /dev/null 2>&1 || true
  update-ca-certificates --fresh
elif [ -f /mnt/zen/certs/$domain/ca.cer ]; then
  echo "Copying SSL certificates"
  cp /mnt/zen/certs/$domain/ca.cer /usr/local/share/ca-certificates/ca.crt > /dev/null 2>&1 || true
  update-ca-certificates --fresh
fi


################################################################################
### Finish setup and run the node

# Setup proper ownership on the created files and folders
chown -R zenops:zenops $HOME /mnt/zen

gosu zenops zen-fetch-params

echo "Starting $@ ..."
if [[ "$1" == zend ]]; then
  gosu zenops /bin/bash -c "$@ $OPTS"
fi

gosu zenops "$@"