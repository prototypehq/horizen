#!/bin/bash

# link the secure node tracker config, bail if not present
if [ -f "/mnt/zen/tracker/config.json" ]; then
  echo "Linking node tracker configuration..."
  ln -s /mnt/zen/tracker /home/node/nodetracker/config > /dev/null 2>&1 || true
else
  echo "Node tracker config not found. Exiting."
  exit 1
fi

# Copy the zencash params
cp -r /mnt/zen/zcash-params /mnt/zcash-params

# Fix permissons
chown -R node:node /mnt/zen/tracker /mnt/zcash-params /home/node/nodetracker
chmod g+rw /mnt/zen/tracker /home/node/nodetracker
chmod -R 777 /home/node/nodetracker/config

cd /home/node/nodetracker
gosu node node app.js